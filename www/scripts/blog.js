﻿$(document).ready(function () {

    show_loading();
    showBlogs();

    setTimeout(function () { hide_loading(); }, 2000);

    $(".content-page").on('click', '.link_view', function () {
        get_blog_single($(this).data('blog_id'), 0);
    });

    $(".content-page").on('click', '.link_comment', function () {
        get_blog_single($(this).data('blog_id'), 1);
    });

});