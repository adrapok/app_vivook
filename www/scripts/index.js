﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        var parentElement = document.getElementById('deviceready');
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');
        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        $.support.cors = true;
        var storage = window.localStorage;

        $.support.cors = true;

        $("#login_btn").on('click', function () {
            token = '4dr4p0k4';

            storage.setItem('token', token);

            window.location.replace('index.html');

        });

    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();
    /**
Funciones para llenar datos... a darle.
*/

    var storage = window.localStorage;
    if (storage.getItem('token') === null)
        window.location.replace('login.html');

    var langs = ['en', 'es'];
    var langCode = '';
    var lang = 'es';
    $(document).ready(function () {
        //traslate_site();
        $("#menu_logout").on('click', function () {
            show_loading();
            storage.removeItem('token');
            window.location.replace('login.html');
        });

        $("#btn_add_comment").on("click", function () {
            add_comment();
        });

    });

    var translate = function (jsdata) {
        $("[tkey]").each(function (index) {
            var strTr = jsdata[$(this).attr('tkey')];
            $(this).html(strTr);
        });
    }

    langCode = navigator.language.substr(0, 2);

    if (langCode in langs)
        $.getJSON('../lang/' + langCode + '.json', translate);
    else
        $.getJSON('../lang/es.json', translate);

    function show_loading() {
        $('.preloader-outer').css('display', 'block');
    }

    function hide_loading() {
        $('.preloader-outer').css('display', 'none');
    }

    function showBlogs() {
        $('.content-page').html('');
        $.ajax({
            url: "http://localhost/testeando/jsonAppVivook/blog.php",
            dataType: 'json',
            success: function ( data ) {
                $.each(data, function (i, v) {
                    paragraph = '<p>' + v.author + ' - ' + v.date + ' | <a class="blog_section" data-sectid="' + v.section_id + '"> ' + v.blog_name + '</a></p>';
                    div_clone = $('#div_base').clone();
                    div_clone.find('h2 .blog_title').data('blog', v.blog_id);
                    div_clone.find('h2 .blog_title').html(v.blog_name);
                    div_clone.find('.authoring').html(paragraph);
                    div_clone.find('.post-content').html('<p>' + v.sumary + '</p>');
                    div_clone.find('.labels').find('.comments-count').html('<a class="link_view"- data-blog_id="' + v.blog_id + '" ><span class="badge"><span class="glyphicon glyphicon-eye-open"></span> Ver</span></a>');
                    div_clone.find('.more-links').find('.add_comment').html('<a class="link_comment" data-blog_id="' + v.blog_id + '" ><span class="badge"><span class="glyphicon glyphicon-comment"></span>     ' + v.comments + '</span></a>');
                    div_clone.removeAttr('style');
                    div_clone.removeAttr("id");
                    div_clone.appendTo("#content-page");
                });
            },
            error: function () {

            }
        });
    }

    function get_blog_single(blog_id, comments) {

        $.ajax({
            url: 'http://localhost/testeando/jsonAppVivook/blog_single.php',
            dataType: 'json',
            beforeSend: function () {
                show_loading();
                $(".comment").remove();
            },
            success: function (data) {
                $("#title_blog").html(data.blog_name);
                $("#author_comment").html(data.author);
                $("#date_comment").html(data.date);
                $("#post_content").html(data.blog);

                $.each(data.comments, function (i, v) {
                    div_comment(v.author, v.date, v.comment);  
                })

                $("#input_comment_add").data("blog_id", blog_id);
                $("#this_show_blog").trigger('click');

                if (comments == 1)
                    setTimeout(function () { $('#input_comment_add').focus(); }, 1000);

                hide_loading();
            },
            error: function () {
                alert('error');
            }
        });       
    }

    function get_edo_cnt() {

        $.ajax({
            url: "http://localhost/testeando/jsonAppVivook/cuenta.php",
            dataType:'json',
            success: function (data) {
                $("#account_condo").html(data.condominio + " - " + data.apto);
                $("#account_sfinal").html(data.saldofinal);
                $.each(data.data, function (i, v) {
                    div_header = "<div class='resulset-header col-xs-12 col-lg-12'>";
                    div_header += "  <div class='resulset-header-row col-xs-6 col-lg-6'>Concepto</div>";
                    div_header += "  <div class='resulset-header-row col-xs-6 col-lg-6'>Monto</div>";
                    div_header += "</div>";


                    if (v.tipo == 'abono')
                        $('#collapseTwo').find('.resulset-header').after(process_div_abono(v, i));

                    if (v.tipo == 'adeudo')
                        $('#collapseOne').find('.resulset-header').after(process_div_adeudo(v, i));

                });
                /*
                $('.row-saldo-inicial').html(data.saldoinicial);
                $('.row-saldo-final').html(data.saldofinal);

                if (data.signosaldoinicial == '-')
                    $('<div />', { 'class': 'row-saldo-inicial saldo-negativo' }).html(data.saldoinicial).appendTo('.content-page');

                if (data.signosaldoinicial == '+')
                    $('<div />', { 'class': 'row-saldo-inicial saldo-favor' }).html(data.saldoinicial).appendTo('.content-page');

                if (data.signosaldoinicial == 0)
                    $('<div />', { 'class': 'row-saldo-inicial saldo-cero' }).html(data.saldoinicial).appendTo('.content-page');

                if (data.signosaldofinal == '-')
                    $('<div />', { 'class': 'row-saldo-final saldo-negativo' }).html(data.saldofinal).appendTo('.content-page');

                if (data.signosaldofinal == '+')
                    $('<div />', { 'class': 'row-saldo-final saldo-favor' }).html(data.saldofinal).appendTo('.content-page');

                if (data.signosaldofinal == 0)
                    $('<div />', { 'class': 'row-saldo-final saldo-cero' }).html(data.saldofinal).appendTo('.content-page');

                hide_this($(".row-saldo-final"));
                hide_this($(".row-saldo-inicial"));
                */
            },
            error: function () {
                console.log('errorsin');
            }
        });

    }

    function process_div_adeudo(data, x) {
        clone_adeudo = $("#edocnt_clone_adeudo").clone();

        clone_adeudo.find('.row-title').html(data.concepto + '<br><span class="row-date">' + data.fecha + '</span>');
        clone_adeudo.find('.resulset-row-importe').html(data.importe + ' <span class="glyphicon glyphicon-fullscreen" data-toggle="modal" data-target="#myModal' + x + '"></span>');
        clone_adeudo.find('.modal').attr('id', 'myModal' + x);
        clone_adeudo.find('.modal-title').html(data.concepto);
        clone_adeudo.find('.date-small').html('Fecha del adeudo: ' + data.fecha);
        clone_adeudo.find('.table-gral-adeudo>tbody').remove();

        tbody = "<tbody>";
        tbody += "  <tr><td class='tdright'>Importe:</td><td class='tdleft'>" + data.importe + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Recargos:</td><td class='tdleft'>" + data.recargo + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Total con recargos:</td><td class='tdleft'>" + data.trecargo + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Descuento:</td><td class='tdleft'>" + data.descuento + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Total ingresos:</td><td class='tdleft'>" + data.ingreso + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Saldo:</td><td class='tdleft'>" + data.saldo + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Clasificación presupuestal:</td><td class='tdleft'>" + data.clasificacion + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Fondo:</td><td class='tdleft'>" + data.fondo + "</td></tr>";
        tbody += "</tbody>";

        clone_adeudo.find('.table-gral-adeudo').html(tbody)

        if (data.aplicaciones && data.aplicado != '') {
            table = "<h4>Aplicación</h4>";
            table += "<table class='table table-striped table_aplicaciones'>";
            table += "<thead>";
            table += "  <tr>";
            table += "      <th>Concepto</th>";
            table += "      <th>Aplicaciones</th>";
            table += "  </tr>";
            table += "</thead>";
            tbody = "<tbody>";
            $.each(data.aplicaciones, function (i, val) {

                tbody += "  <tr>";
                tbody += "      <td>" + val.concepto + "</td>";
                tbody += "       <td>";
                tbody += "          <span class='abono'>" + val.abono + " <span class='glyphicon glyphicon-plus-sign more'></span></span><br>";
                tbody += "           <span class='aplicado'>" + val.aplicado + " <span class='glyphicon glyphicon-ok-sign'></span></span>";

                if (val.cargo != 0) {
                    tbody += "           <br><span class='aplicado'>" + val.cargo + " <span class='glyphicon glyphicon-minus-sign'></span></span>";
                }

                tbody += "      </td>";
                tbody += "   </tr>";
            });

            tbody += "</tbody>";
            table += tbody;

            tfoot = "<tfoot>";
            tfoot += "  <tr><td colspan='4'>Total aplicado: " + data.aplicado + "</td></tr>";
            tfoot += "</tfoot>";

            table += tfoot;
            table += '</table>';

            clone_adeudo.find('.modal-body').append(table);
        }

        clone_adeudo.removeAttr('id');
        clone_adeudo.removeAttr('style');

        return clone_adeudo;
    }

    function process_div_abono(data, x) {
        clone_abono = $("#edocnt_clone_abono").clone();

        clone_abono.find('.row-title').html(data.depto + '<br> <span class="row-date">' + data.fecha + '</span>');
        clone_abono.find('.resulset-row-importe').html(data.monto + ' <span class="glyphicon glyphicon-fullscreen" data-toggle="modal" data-target="#myModal' + x + '"></span>');
        clone_abono.find('.modal').attr('id', 'myModal' + x);
        clone_abono.find('.modal-title').html(data.depto);

        tbody = "<tbody>";
        tbody += "  <tr><td class='tdright'>Monto:</td> <td class='tdleft'>" + data.monto + "</td></tr>";
        tbody += "  <tr><td class='tdright'>Forma de pago:</td><td class='tdleft'>" + data.pago + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Referencia:</td><td class='tdleft'>" + data.referencia + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Fecha movimiento:</td><td class='tdleft'>" + data.fecha + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Cuenta bancaria:</td><td class='tdleft'>" + data.cuenta + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Fondo:</td><td class='tdleft'>" + data.fondo + "</td></tr>";
        tbody += "   <tr><td class='tdright'>Comentarios:</td><td class='tdleft'>" + data.comment + "</td></tr>";
        tbody += "</tbody>";

        clone_abono.find('.table-gral-abono>tbody').remove();
        clone_abono.find('.table-gral-abono').append(tbody);

        if (data.aplicaciones && data.aplicado != '') {
            table = "<h4>Aplicación</h4>";
            table += "<table class='table table-striped table_aplicaciones'>";
            table += "<thead>";
            table += "  <tr>";
            table += "      <th>Concepto</th>";
            table += "      <th>Aplicaciones</th>";
            table += "  </tr>";
            table += "</thead>";
            tbody = "<tbody>";

            $.each(data.aplicaciones, function (i, val) {

                tbody += "  <tr>";
                tbody += "      <td>" + val.concepto + "</td>";
                tbody += "       <td>";
                tbody += "          <span class='abono'>" + val.abono + " <span class='glyphicon glyphicon-plus-sign more'></span></span><br>";
                tbody += "           <span class='aplicado'>" + val.aplicado + " <span class='glyphicon glyphicon-ok-sign'></span></span>";

                if (val.cargo != 0) {
                    tbody += "           <br><span class='aplicado'>" + val.cargo + " <span class='glyphicon glyphicon-minus-sign'></span></span>";
                }

                tbody += "      </td>";
                tbody += "   </tr>";
            });

            tbody += "</tbody>";
            table += tbody;

            tfoot = "<tfoot>";
            tfoot += "  <tr><td colspan='4'>Total aplicado: " + data.aplicado + "</td></tr>";
            tfoot += "</tfoot>";

            table += tfoot;
            table += '</table>';

            clone_abono.find('.modal-body').append(table);
        }

        clone_abono.removeAttr('id');
        clone_abono.removeAttr('style');
        return clone_abono;
    }
    
    function add_comment() {
        blog_id = $("#input_comment_add").data('blog_id');
        comment = $("#input_comment_add").val();
        author  = "Pendiente";
        date = new Date('Y-m-d');

        $.ajax({
            url: 'http://localhost/testeando/jsonAppVivook/blog_single.php',
            data: { 'blog_id': blog_id, 'comment': comment},
            type: 'post',
            beforeSend: function () {
                show_loading();
            },
            success: function (data) {
                data.author = 'Ala Este-Admin';
                data.date = '26/sep/2016 01=>45 PM';

                div_comment(data.author, data.date, comment);
                $("#in  put_comment_add").val('');
                hide_loading();
            },
            error: function () {
                alert('error');
            }
        });
    }

    function div_comment(author, date, comment) {
        div = "<div class='comment'>";
        div += "<div class='comment-by'>By:  " + author + " A quien corresponda</div>";
        div += "<div class='comment-date'>" + date + "</div>";
        div += "<div class='comment-content'>" + comment + "</div>";
        div += "</div>";

        $("#comments").append(div);
    }

    function get_profile() {
        $.ajax({
            url: "http://localhost/testeando/jsonAppVivook/profile.php",
            dataType: 'json',
            beforeSend: function () {
                show_loading();
            },
            success: function (data) {

                $("#profile_name").val(data.name);
                $("#profile_condo").val(data.condo);

                if (data.notification == 1) {
                    $("#profile_notification").prop("checked", true);

                } else {
                    $("#profile_notification").prop("checked", false);
                }

                $.each(data.condos, function (indx, condo) {
                    if( condo.condo_id == data.condo_id)
                        $("#profile_condos").append("<option value='" + condo.condo_id + "' selected='selected' >" + condo.name + "</option>");
                    else
                        $("#profile_condos").append("<option value='" + condo.condo_id + "' >" + condo.name + "</option>");
                });

                $.each(data.languages, function (i, lang) {
                    if(i == data.language)
                        $("#profile_language").append("<option value='" + i + "' selected='selected'>" + lang + "</option>")
                    else
                        $("#profile_language").append("<option value='" + i + "'>" + lang + "</option>")
                });


            },
            error: function () {
                console.log('hay un error, checale cuate');
            }
        });
    }
